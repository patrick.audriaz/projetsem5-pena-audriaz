import React from "react";

const Error = () => (
    <div className="App notfound-bg">
      <div className="notfound-container">
        <h1 className="notfound-h1">
          <b>
            404 - Not found <span>:(</span>
          </b>
        </h1>
        <p>Désolé, mais la page que vous essayez d'atteindre n'existe pas.</p>
        <p>Cela peut être la cause de : </p>
        <ul>
          <li>&emsp;• Fausse URL (adresse)</li>
          <li>&emsp;• Lien expiré</li>
        </ul>
      </div>
    </div>
  );

// export component to make it available to import
export default Error;
