import React, { Component } from "react";
import Fab from "@material-ui/core/Fab";
import Icon from "@material-ui/core/Icon";
import { withStyles } from "@material-ui/core/styles";
import { NavLink } from "react-router-dom";

const styles = theme => ({
  button: {
    color: "#64bda4",
    background: "white",
    "&:hover": {
      background: "#e5e5e5"
    }
  }
});

class Home extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className="App Home">
        <NavLink to="/driver" className="home-item">
          <span>
            <Fab classes={{ root: classes.button }}>
              <Icon>directions_car</Icon>
            </Fab>
            <div className="home-label">Conducteur</div>
          </span>
        </NavLink>
        <NavLink to="/passenger" className="home-item">
          <span>
            <Fab classes={{ root: classes.button }}>
              <Icon>person</Icon>
            </Fab>
            <div className="home-label">Passager</div>
          </span>
        </NavLink>
        <NavLink to="/test" className="home-item">
          <span>
            <Fab classes={{ root: classes.button }}>
              <Icon>build</Icon>
            </Fab>
            <div className="home-label">Tests</div>
          </span>
        </NavLink>
      </div>
    );
  }
}

export default withStyles(styles)(Home);
