import React from "react";
import train_image from "../assets/train.png";

const PassengerDestinationPicked = () => (
    <div className="reservation-content">
      <div className="passenger-trajets">
        <div className="container">
          <div className="row">
            <div className="col s12 m10 l10" />
            <div className="col s12 m10 l10 offset-l1 offset-m1">
              <div className="driver-label">DESTINATION</div>
              <div className="col s12 m12 l12 center">
                <img
                  src={train_image}
                  alt="gare belfaux cff"
                  className="passenger-dest-img"
                />
                <h5 className="destination-label">Gare de Belfaux Village</h5>
                <div className="mytag">
                  <div className="driver-label white-text">DEPART</div>
                </div>
              </div>
            </div>
            <div className="col s12 m12 l4" />
          </div>
        </div>
      </div>
    </div>
);

export default PassengerDestinationPicked;
