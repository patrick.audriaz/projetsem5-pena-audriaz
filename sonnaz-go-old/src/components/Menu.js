import React, {Component} from "react";
import logo from "../assets/logo.png";
import { NavItem, Navbar } from "react-materialize";
import { NavLink } from "react-router-dom";

const Img = <img src={logo} className="brand-img" alt={"Sonnaz Go"} />;

export default class Menu extends Component {
  constructor(props) {
        super(props);
        this.state = {
          isDriver: false
        };
    }

  componentDidMount(){
    if((window.location.href.toString()) === 'http://localhost:3000/passenger'){
      this.setState(() => ({
        isDriver: true
      }));
      console.log(this.state.isDriver);
    }else if((window.location.href.toString()) === 'http://localhost:3000/driver'){
       this.setState(() => ({
         isDriver: false
       }));
    }
    };
  getIsDriver(){
    return this.state.isDriver;
  };
  render() {
    return (
      <div className="driver-menu">
        <div className="row">
          <div className="col s10 m11 l11">
            <Navbar className="nav" brand={Img} right>
              <NavItem href="/profil"> Modifier mon profil </NavItem>
              <NavItem href="/contact"> Contacter l'admin </NavItem>
              <NavItem href="/">Déconnexion</NavItem>
            </Navbar>
          </div>
          <div className="col s2 m1 l1">
            <NavLink to={this.props.toLink} >
              <div className={this.props.css}>
                <img
                  src={this.props.src}
                  width={this.props.size}
                  alt={this.props.alt}  
                />
              </div>
            </NavLink>
          </div>
        </div>
      </div>
    );
  };
}