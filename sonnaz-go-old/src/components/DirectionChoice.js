import React from "react";
import ChoiceButton from "./ChoiceButton";

const DirectionChoice = () => (
      <div>
        <div className="direction">
          <div className="direction-label ">DIRECTION ?</div>
        </div>
        <ChoiceButton />
      </div>
);

// export component to make it available to import
export default DirectionChoice;
