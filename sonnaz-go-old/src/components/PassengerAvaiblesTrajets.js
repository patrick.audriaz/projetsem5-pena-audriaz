import React from "react";
import { Button } from "react-materialize";

const PassengerAvaiblesTrajets = () => (
    <div className="row">
      <div className="col s12 m10 l10" />
      <div className="col s12 m10 l10 offset-l1 offset-m1">
        <div className="driver-label ">TRAJETS DISPONIBLES</div>
        <div className="center driver-item">
          <h2 className="white-text driver-item-label">Vers Gare de Belfaux</h2>
          <p className="white-text">9h00 - 9h30</p>
          <Button waves="light" className="black-text driver-item-button">
            Détails
          </Button>
        </div>
        <div className="center driver-item">
          <h2 className="white-text driver-item-label">Vers Gare de Belfaux</h2>
          <p className="white-text">9h00 - 9h30</p>
          <Button waves="light" className="black-text driver-item-button">
            Détails
          </Button>
        </div>
        <div className="center driver-item">
          <h2 className="white-text driver-item-label">Vers Gare de Belfaux</h2>
          <p className="white-text">9h00 - 9h30</p>
          <Button waves="light" className="black-text driver-item-button">
            Détails
          </Button>
        </div>
      </div>
      <div className="col s12 m12 l4" />
    </div>  
);

export default PassengerAvaiblesTrajets;
