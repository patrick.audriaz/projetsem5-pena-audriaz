import React from "react";
import ReservationHeader from "./ReservationHeader";
import ButtonNext from "./ButtonNext";
import Map from "./Map";
import { NavLink } from "react-router-dom";
import passenger_image from "../assets/passenger.svg";
import Menu from "../components/Menu";

const DriverRide1 = () => (
    <div className="App container">
      <div className="reservation-wrapper">
        <Menu
          src={passenger_image}
          toLink="/passenger"
          size="40"
          alt="passenger_image"
          css="driver-icon"
        />
        <div className="reservation-header">
          <div className="col s2 m1 l1">
            <NavLink className="reservation-back" to="/driver/newride">
              <ReservationHeader />
            </NavLink>
          </div>
        </div>
        <div className="desservi ">ARRETS DESSERVIS ?</div>
        <div className="reservation-map">
          <Map />
        </div>
      </div>
      <NavLink to="/driver/newride2">
        <ButtonNext />
      </NavLink>
    </div>
);

// export component to make it available to import
export default DriverRide1;
