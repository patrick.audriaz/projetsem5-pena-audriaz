import React from "react";
import { Button } from "react-materialize";
import profil_image from "../assets/bob.png";
import train_image from "../assets/train.png";

const PassengerItem = () => (
    <div className="col s12 m12 l12">
      <div className="col s3 m3 l3 ">
        <img src={profil_image} alt="" className="image-item" width="120" />
        <h6 className="white-text">Bob le conducteur</h6>
      </div>
      <div className="col s6 m6 l6">
        <h2 className="white-text text-responsive">Vers Gare de Belfaux</h2>
        <p className="white-text">9h00 - 9h30</p>
        <Button
          waves="light"
          className="passenger-item-button black-text"
        >
          Détails
        </Button>
      </div>
      <div className="col s3 m3 l3">
        <img src={train_image} alt="" className="image-item2" width="120" />
        <h6 className="white-text">Gare de Belfaux CFF</h6>
      </div>
    </div>
);

export default PassengerItem;
