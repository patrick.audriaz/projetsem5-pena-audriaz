import React from "react";
import PassengerItem from "./PassengerItem";
import SwipeableViews from "react-swipeable-views";

const PassengerTrajets = () => (
    <div className="passenger-content">
      <div className="passenger-trajets">
        <div className="container">
          <div className="row">
            <div className="col s12 m10 l10" />
            <div className="col s12 m10 l10 offset-l1 offset-m1">
              <div className="driver-label">MES PROCHAINS TRAJETS</div>
              <div className="mycarousel">
                <SwipeableViews enableMouseEvents>
                  <div className="center">
                    <PassengerItem />
                  </div>
                  <div className="center">
                    <PassengerItem />
                  </div>
                  <div className="center">
                    <PassengerItem />
                  </div>
                </SwipeableViews>
              </div>
            </div>
            <div className="col s12 m12 l4" />
          </div>
        </div>
      </div>
    </div>
);

export default PassengerTrajets;
