import React, { Component } from "react";
import { Button } from "react-materialize";
import mapboxgl from "mapbox-gl";

mapboxgl.accessToken =
  "pk.eyJ1IjoiamFja2VubiIsImEiOiJjanE2am1tZ3gxOHY1NDNxaDR4dGFqYXo5In0.kGiYv0GCpu_kH9H1t0bHUg";

export default class Map extends Component {
  state = {
      lng: 7.120337,
      lat: 46.828371,
      zoom: 12.5
  };
  componentDidMount() {
    var geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: "Point",
            coordinates: [7.112046, 46.830576]
          },
          properties: {
            title: "Administration communale"
          }
        },
        {
          type: "Feature",
          geometry: {
            type: "Point",
            coordinates: [7.099866, 46.838253]
          },
          properties: {
            title: "Le Sarrazin"
          }
        }
      ]
    };

    const { lng, lat, zoom } = this.state;
    const map = new mapboxgl.Map({
      container: this.mapContainer,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom
    });

    map.on("move", () => {
      const { lng, lat } = map.getCenter();

      this.setState({
        lng: lng.toFixed(4),
        lat: lat.toFixed(4),
        zoom: map.getZoom().toFixed(2)
      });
    });

    // Add zoom and rotation controls to the map.
    map.addControl(new mapboxgl.NavigationControl());

    // Add geolocate control to the map.
    map.addControl(
      new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
      })
    );

    document.getElementById("fit").addEventListener("click", function() {
      map.flyTo({ center: [lng, lat], zoom: zoom });
    });

    map.setStyle("mapbox://styles/jackenn/cjq6js6u3blli2rqaiviaeqcw");

    // add markers to map
    geojson.features.forEach(function(marker) {
      // create a HTML element for each feature
      var el = document.createElement("div");
      el.className = "marker";

      // make a marker for each feature and add to the map
      new mapboxgl.Marker(el).setLngLat(marker.geometry.coordinates).addTo(map);
      new mapboxgl.Marker(el)
        .setLngLat(marker.geometry.coordinates)
        .setPopup(
          new mapboxgl.Popup({ offset: 25, closeButton: false }) // add popups
            .setHTML(
              '<p class="popup-text">' +
                marker.properties.title +
                '<button class="select-marker"><br>Selectionner</button>' +
                "<p/>"
            )
        )
        .addTo(map);
    });
  };
  render() {
    return (
      <div className="map">
        <div
          ref={el => (this.mapContainer = el)}
          className=" driver-map absolute right left top bottom"
        >
          <Button
            waves="light"
            className="black-text driver-map-button"
            id="fit"
          >
            Centrer
          </Button>
        </div>
      </div>
    );
  };
}
