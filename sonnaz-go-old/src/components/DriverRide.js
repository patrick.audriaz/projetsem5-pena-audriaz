import React from "react";
import DirectionChoice from "./DirectionChoice";
import ReservationHeader from "./ReservationHeader";
import ButtonNext from "./ButtonNext";
import { NavLink } from "react-router-dom";
import passenger_image from "../assets/passenger.svg";
import Menu from "../components/Menu";

const DriverRide = () => (
    <div className="App container">
      <div className="reservation-wrapper">
        <Menu
          src={passenger_image}
          toLink="/passenger"
          size="40"
          alt="passenger_image"
          css="driver-icon"
        />
        <div className="reservation-header">
          <div className="col s2 m1 l1">
            <NavLink className="reservation-back" to="/driver">
              <ReservationHeader />
            </NavLink>
          </div>
        </div>
        <DirectionChoice />
      </div>
      <NavLink to="/driver/newride1">
        <ButtonNext />
      </NavLink>
    </div>
);

// export component to make it available to import
export default DriverRide;
