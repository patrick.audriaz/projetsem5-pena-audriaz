import React from "react";
import { Button } from "react-materialize";
import Zoom from "react-reveal/Zoom";
import { NavLink } from "react-router-dom";

const DriverNewTrajet = () => (
    <div className="driver-new">
      <div className="row">
        <div className="col s12 m12 l4" />
        <div className="col s12 m12 l4 center">
          <Zoom>
            <h2 className="driver-label">PROPOSER UN TRAJET</h2>
            <NavLink to="/driver/newride">
              <Button
                waves="light"
                className="black-text driver-new-button"
                floating
                large
                icon="add"
              />
            </NavLink>
          </Zoom>
        </div>
        <div className="col s12 m12 l4" />
      </div>
    </div>
);

export default DriverNewTrajet;
