import React from "react";
import { Icon } from "react-materialize";

const ReservationHeader = () => (
    <Icon small className="reservation-back">
      keyboard_arrow_left
    </Icon>
);

export default ReservationHeader;
