import React from "react";
import { Button } from "react-materialize";

const ButtonNext = () => (
      <div>
        <Button
          floating
          large
          className="reservation-next right bottom"
          icon="arrow_forward"
        />
      </div>
);

// export component to make it available to import
export default ButtonNext;
