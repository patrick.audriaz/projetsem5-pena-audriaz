import React, { Component } from "react";
import { Button, Icon } from "react-materialize";
import Rides from "../data/Rides.json";

export default class DriverItem extends Component {
  render() {
    return (
      <div className="center">
        {Rides.map(ridesDetails => {
          return (
            <div key={ridesDetails.id} className="center driver-item">
              {ridesDetails.direction === "Partir de la Sonnaz" ? (
                <div>
                  <Icon className="white-text driver-item-icon">
                    time_to_leave
                  </Icon>
                  <p className="white-text driver-item-label-b">
                    En direction de :{" "}
                  </p>
                  <span className="white-text driver-item-label">
                    {ridesDetails.outsideDest}
                  </span>
                  <p className="white-text driver-item-label-b">
                    Arrêts desservis :
                  </p>

                  <div className="white-text driver-item-label">
                    {ridesDetails.insideDests.map(item => {
                      return <div key={item.id}>{item.name}</div>;
                    })}
                  </div>
                </div>
              ) : (
                <div>
                  <Icon className="white-text driver-item-icon">home</Icon>
                  <p className="white-text driver-item-label-b">
                    En direction de :{" "}
                  </p>
                  <div className="white-text driver-item-label">
                    {ridesDetails.insideDests.map(item => {
                      return <div key={item.id}>{item.name}</div>;
                    })}
                  </div>
                  <p className="white-text driver-item-label-b">
                    Arrêt desservi :
                  </p>
                  <p>
                    <span className="white-text driver-item-label">
                      {ridesDetails.outsideDest}
                    </span>
                  </p>
                </div>
              )}
              <p className="white-text driver-item-label-t">
                {" "}
                <span className="white-text driver-item-label">
                  {ridesDetails.day}
                </span>
              </p>
              <p className="white-text driver-item-label">
                {" "}
                <span className="white-text driver-item-label">
                  <Icon className="white-text driver-item-icon">
                    access_time
                  </Icon>{" "}
                  {ridesDetails.time}
                </span>
              </p>
              <p className="white-text driver-item-label">
                <span className="white-text driver-item-label">
                  <Icon className="white-text driver-item-icon">person</Icon>{" "}
                  {ridesDetails.nbPassengers}
                </span>
              </p>
              <Button waves="light" className="black-text driver-item-button">
                Supprimer
              </Button>
            </div>
          );
        })}
      </div>
    );
  }
}
