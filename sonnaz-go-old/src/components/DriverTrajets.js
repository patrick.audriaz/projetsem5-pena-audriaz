import React from "react";
import DriverItem from "./DriverItem";

const DriverTrajets = () => (
  <div className="driver-content">
    <div className="driver-trajets">
      <div className="container">
        <div className="row">
          <div className="col s12 m10 l10" />
          <div className="col s12 m10 l10 offset-l1 offset-m1">
            <div className="driver-label ">MES PROCHAINS TRAJETS</div>
            <DriverItem />
          </div>
          <div className="col s12 m12 l4" />
        </div>
      </div>
    </div>
  </div>
);

export default DriverTrajets;
