import React from "react";
import bus_image from "../assets/bus.png";
import train_image from "../assets/train.png";
import { NavLink } from "react-router-dom";
import { Zoom } from "react-reveal";

const DriverDestinations = () => (
    <div className=" driver-destination">
      <div className="row">
        <div className="container">
          <Zoom>
            <div className="col s6 m4 l4 offset-l2 offset-m2 center">
              <NavLink to="/driver">
                <img
                  src={train_image}
                  alt="gare belfaux cff"
                  className="passenger-dest-img"
                />
                <h5 className="destination-label">Gare de Belfaux CFF</h5>
              </NavLink>
            </div>
            <div className="col s6 m4 l4 center">
              <NavLink to="/driver">
                <img
                  src={train_image}
                  alt="gare belfaux viallage"
                  className="passenger-dest-img"
                />
                <h5 className="destination-label">Gare de Belfaux Village</h5>
              </NavLink>
            </div>
            <div className="col s6 m4 l4 offset-l2 offset-m2 center">
              <NavLink to="/driver">
                <img
                  src={train_image}
                  alt="gare pensier"
                  className="passenger-dest-img"
                />
                <h5 className="destination-label">Gare de Pensier</h5>
              </NavLink>
            </div>
            <div className="col s6 m4 l4 center">
              <NavLink to="/driver">
                <img
                  src={bus_image}
                  alt="portes de fribourg"
                  className="passenger-dest-img"
                />
                <h5 className="destination-label">Arrêt Portes de Fribourg</h5>
              </NavLink>
            </div>
          </Zoom>
        </div>
      </div>
    </div>
);

export default DriverDestinations;
