import React from "react";
import ReservationHeader from "./ReservationHeader";
import ButtonSave from "./ButtonSave";
import { NavLink } from "react-router-dom";
import passenger_image from "../assets/passenger.svg";
import Menu from "../components/Menu";
import { Row, Input, Icon } from "react-materialize";

const DriverRide2 = () => (
    <div className="App container">
      <div className="reservation-wrapper">
        <Menu
          src={passenger_image}
          toLink="/passenger"
          size="40"
          alt="passenger_image"
          css="driver-icon"
        />
        <div className="reservation-header">
          <div className="col s2 m1 l1">
            <NavLink className="reservation-back" to="/driver/newride2">
              <ReservationHeader />
            </NavLink>
          </div>
        </div>
        <div className="destination ">HORAIRE ?</div>
        <div className="driver-form">
          <Row className="driver-form-item">
            <Input
              s={12}
              label="Date"
              name="on"
              type="date"
              onChange={function(e, value) {}}
            >
              <Icon>date_range</Icon>
            </Input>
          </Row>
          <Row className="driver-form-item">
            <Input
              s={12}
              label="Heure"
              name="on"
              type="time"
              onChange={function(e, value) {}}
            >
              <Icon>watch_later</Icon>
            </Input>
          </Row>
          <div className="driver-form-range-label">Nombre de places</div>
          <Row className="driver-form-range">
            <Input
              s={12}
              type="range"
              min="1"
              max="6"
              onChange={function(e, value) {}}
            />
          </Row>
        </div>
      </div>

      <NavLink to="/driver">
        <ButtonSave />
      </NavLink>
    </div>
);

// export component to make it available to import
export default DriverRide2;
