import React from "react";
import { Button } from "react-materialize";

const ButtonSave = () => (
      <div>
        <Button
          floating
          large
          className="large reservation-next right bottom"
          icon="save"
        />
      </div>
);

// export component to make it available to import
export default ButtonSave;
