import React from "react";
import bus_image from "../assets/bus.png";
import train_image from "../assets/train.png";
import { NavLink } from "react-router-dom";

const PassengerDestinations = () => (
    <div className="passenger-new">
      <div className="row trajets">
        <div className="container">
          <div className="passenger-label">RECHERCHER UN TRAJET</div>
          <div className="col s6 m4 l4 offset-l2 offset-m2 center">
            <NavLink to="/passenger/reservation1">
              <img
                src={train_image}
                alt="gare belfaux cff"
                className="passenger-dest-img"
              />
              <h5 className="destination-label">Gare de Belfaux CFF</h5>
            </NavLink>
          </div>
          <div className="col s6 m4 l4 center">
            <NavLink to="/passenger/reservation1">
              <img
                src={train_image}
                alt="gare belfaux viallage"
                className="passenger-dest-img"
              />
              <h5 className="destination-label">Gare de Belfaux Village</h5>
            </NavLink>
          </div>
          <div className="col s6 m4 l4 offset-l2 offset-m2 center">
            <NavLink to="/passenger/reservation1">
              <img
                src={train_image}
                alt="gare pensier"
                className="passenger-dest-img"
              />
              <h5 className="destination-label">Gare de Pensier</h5>
            </NavLink>
          </div>
          <div className="col s6 m4 l4 center">
            <NavLink to="/passenger/reservation1">
              <img
                src={bus_image}
                alt="portes de fribourg"
                className="passenger-dest-img"
              />
              <h5 className="destination-label">Arrêt Portes de Fribourg</h5>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
);

export default PassengerDestinations;
