import React from "react";
import "../App.css";
import Menu from "./Menu";
import { Breadcrumb, MenuItem } from "react-materialize";
import { NavLink } from "react-router-dom";
import PassengerAvaiblesTrajets from "./PassengerAvaiblesTrajets";
import driver_image from "../assets/driver.svg";
import ButtonSave from "./ButtonSave";

const PassengerReservation2 = () => (
    <div className="App container Passenger">
      <Menu
        src={driver_image}
        toLink="/driver"
        size="37"
        alt="driver_image"
        css="passenger-icon"
      />
      <div className="passenger-avaibles-trajets">
        <div className="container center add-padding passenger-breadcrumb">
          <Breadcrumb className="menu-reservation">
            <NavLink to="/passenger/reservation1">
              <i className="large material-icons breadcrumbs-arrow">
                arrow_back
              </i>
            </NavLink>
            <MenuItem>Gare Routière</MenuItem>
            <MenuItem>Point choisi</MenuItem>
          </Breadcrumb>
        </div>
        <PassengerAvaiblesTrajets />
      </div>
      <NavLink to="/passenger">
        <ButtonSave />
      </NavLink>
    </div>
);

export default PassengerReservation2;
