import React from "react";
import "../App.css";
import Menu from "./Menu";
import PassengerDestinationPicked from "./PassengerDestinationPicked";
import ReservationHeader from "./ReservationHeader";
import Map from "./Map";
import { NavLink } from "react-router-dom";
import driver_image from "../assets/driver.svg";
import ButtonNext from "./ButtonNext";

const PassengerReservation1 = () => (
    <div className="App container Passenger">
      <div className="reservation-wrapper">
        <Menu
          src={driver_image}
          toLink="/driver"
          size="37"
          alt="driver_image"
          css="passenger-icon"
        />
        <div className="reservation-header">
          <div className="col s2 m1 l1">
            <NavLink className="reservation-back" to="/passenger">
              <ReservationHeader />
            </NavLink>
          </div>
        </div>
        <PassengerDestinationPicked />
        <div className="passenger-map">
          <Map />
        </div>
      </div>
      <NavLink to="/passenger/reservation2">
        <ButtonNext />
      </NavLink>
    </div>
  );

export default PassengerReservation1;
