import React from "react";
import { Button } from "react-materialize";
import Zoom from "react-reveal/Zoom";

const ChoiceButton = () => (
    <div className="direction-choice">
      <Zoom>
        <span className="direction-button">
          <Button floating large className="choice-button" icon="home" />
          <div className="choice-label">Rentrer à la maison</div>
        </span>
        <span className="direction-button">
          <Button
            floating
            large
            className="choice-button"
            icon="time_to_leave"
          />
          <div className="choice-label">Partir de la Sonnaz</div>
        </span>
      </Zoom>
    </div>
);

// export component to make it available to import
export default ChoiceButton;
