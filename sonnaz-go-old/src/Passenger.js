import React from "react";
import "./App.css";
import Menu from "./components/Menu";
import PassengerTrajets from "./components/PassengerTrajets";
import PassengerDestinations from "./components/PassengerDestinations";
import driver_image from "./assets/driver.svg";

const Passenger = () => (
      <div className="App container Passenger">
        <div className="passenger-wrapper">
          <Menu 
            src = {driver_image} 
            toLink = "/driver" 
            size = "37" 
            alt = "driver_image"
            css = "passenger-icon"
          />
          <PassengerTrajets />
        </div>
        <PassengerDestinations />
      </div>
);

export default Passenger;
