import React from "react";
import "./App.css";
import "./components/css/components.css";
import "./components/css/patrick.css";
import { Route, Switch } from "react-router-dom";
import Driver from "./Driver";
import Passenger from "./Passenger";
import Home from "./Home";
import Error from "./Error";
import Test from "./Test";
import PassengerReservation1 from "./components/PassengerReservation1";
import PassengerReservation2 from "./components/PassengerReservation2";
import Contact from "./Contact";
import Profil from "./Profil";
import DriverRide from "./components/DriverRide";
import DriverRide1 from "./components/DriverRide1";
import DriverRide2 from "./components/DriverRide2";
import DriverRide3 from "./components/DriverRide3";
import { CSSTransition, TransitionGroup } from "react-transition-group";

const App = () => (
  <div>
    <Route
      render={({ location }) => (
        <TransitionGroup>
          <CSSTransition key={location.key} timeout={500} classNames="fade">
            <Switch className="App" location={location}>
              <Route path="/" component={Home} exact />
              <Route path="/driver" component={Driver} exact />
              <Route path="/passenger" component={Passenger} exact />
              <Route path="/test" component={Test} exact />
              <Route path="/contact" component={Contact} exact />
              <Route
                path="/passenger/reservation1"
                component={PassengerReservation1}
                exact
              />
              <Route
                path="/passenger/reservation2"
                component={PassengerReservation2}
                exact
              />
              <Route path="/driver/newride" component={DriverRide} exact />
              <Route path="/driver/newride1" component={DriverRide1} exact />
              <Route path="/driver/newride2" component={DriverRide2} exact />
              <Route path="/driver/newride3" component={DriverRide3} exact />
              <Route path="/profil" component={Profil} exact />
              <Route component={Error} />
            </Switch>
          </CSSTransition>
        </TransitionGroup>
      )}
    />
  </div>
);

export default App;
