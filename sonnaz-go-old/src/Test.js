import React, { Component } from "react";
import { Button, Icon } from "react-materialize";
import Rides from "./data/Rides.json";

export default class Test extends Component {
  render() {
    return (
      <div className="Test">
        <div className="test-title">Test item</div>

        {Rides.map(ridesDetails => {
          return (
            <div key={ridesDetails.id} className="center test-item">
              {ridesDetails.direction === "Partir de la Sonnaz" ? (
                <div>
                  <Icon className="white-text test-item-icon">
                    time_to_leave
                  </Icon>
                  <p className="white-text test-item-label-b">
                    En direction de :{" "}
                  </p>
                  <span className="white-text test-item-label">
                    {ridesDetails.outsideDest}
                  </span>
                  <p className="white-text test-item-label-b">
                    Arrêts desservis :
                  </p>
                  <p>
                    <span className="white-text test-item-label">
                      {ridesDetails.insideDests.map(item => {
                        return <div key={item.id}>{item.name}</div>;
                      })}
                    </span>
                  </p>
                </div>
              ) : (
                <div>
                  <Icon className="white-text test-item-icon">home</Icon>
                  <p className="white-text test-item-label-b">
                    En direction de :{" "}
                  </p>
                  <span className="white-text test-item-label">
                    {ridesDetails.insideDests.map(item => {
                      return <div key={item.id}>{item.name}</div>;
                    })}
                  </span>
                  <p className="white-text test-item-label-b">
                    Arrêt desservi :
                  </p>
                  <p>
                    <span className="white-text test-item-label">
                      {ridesDetails.outsideDest}
                    </span>
                  </p>
                </div>
              )}
              <p className="white-text test-item-label-t">
                {" "}
                <span className="white-text test-item-label">
                  {ridesDetails.day}
                </span>
              </p>
              <p className="white-text test-item-label">
                {" "}
                <span className="white-text test-item-label">
                  Heure de départ : {ridesDetails.time}
                </span>
              </p>
              <p className="white-text test-item-label-p">
                Passagers :{" "}
                <span className="white-text test-item-label">
                  {ridesDetails.nbPassengers}
                </span>
              </p>
              <Button waves="light" className="black-text driver-item-button">
                Supprimer
              </Button>
            </div>
          );
        })}
      </div>
    );
  }
}
