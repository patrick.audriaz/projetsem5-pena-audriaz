import React, {Component} from "react";
import "./App.css";
import Menu from "./components/Menu";
import DriverTrajets from "./components/DriverTrajets";
import DriverNewTrajet from "./components/DriverNewTrajet";
import passenger_image from "./assets/passenger.svg";

export default class Driver extends Component {
  render() {
    return (
      <div className="App driver-medium-small Driver">
        <div className="container">
          <div className="driver-wrapper">
            <Menu 
              src = {passenger_image} 
              toLink = "/passenger" 
              size = "40" 
              alt = "passenger_image"
              css = "driver-icon"
            />
            <DriverTrajets />
          </div>
          <DriverNewTrajet />
        </div>
      </div>
    );
  }
}