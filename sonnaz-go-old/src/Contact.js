import React, {Component} from "react";
import "./App.css";
import Menu from "./components/Menu";
import Formular from "./components/Formular";
import passenger_image from "./assets/passenger.svg";
import driver_image from "./assets/driver.svg";

export default class Contact extends Component {
  state = {
    nav: new Menu()
  };
  render(){
    return (
      <div className="App container">
           {
            (this.state.nav.getIsDriver() === true) &&
            <Menu 
                src = {passenger_image} 
                toLink = "/passenger" 
                size = "40" 
                alt = "passenger_image"
                css = "driver-icon"
            />
          }
          {
            (this.state.nav.getIsDriver() === false ) &&
            <Menu
                src = {driver_image} 
                toLink = "/driver" 
                size = "37" 
                alt = "driver_image"
                css = "passenger-icon"
            />
          }
        <Formular />
      </div>
    );
  }
};
