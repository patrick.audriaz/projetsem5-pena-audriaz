import { createStore, combineReducers } from 'redux';
import ridesReducer from '../reducers/rides';
import filtersReducer from '../reducers/filters';

export default () => {
  const store = createStore(
    combineReducers({
      rides: ridesReducer,
      filters: filtersReducer
    }),
     window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );

  return store;
};
