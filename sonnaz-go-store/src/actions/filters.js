// SET_TEXT_FILTER
export const setTextFilter = (text = '') => ({
  type: 'SET_TEXT_FILTER',
  text
});

// SORT_BY_DATE
export const sortByDate = () => ({
  type: 'SORT_BY_DATE'
});

// SORT_BY_PLACES
export const sortByPlaces = () => ({
  type: 'SORT_BY_PLACES'
});

// SET_START_DATE
export const setStartDate = (startDate) => ({
  type: 'SET_START_DATE',
  startDate
});

// SET_END_DATE
export const setEndDate = (endDate) => ({
  type: 'SET_END_DATE',
  endDate
});

// SET_OUTSIDE_DESTINATION
export const setOutsideDestination = (outside_destination) => ({
  type: 'SET_OUTSIDE_DESTINATION',
  outside_destination
});

// SET_INSIDE_DESTINATION
export const setInsideDestination = (inside_destination) => ({
  type: 'SET_INSIDE_DESTINATION',
  inside_destination
});