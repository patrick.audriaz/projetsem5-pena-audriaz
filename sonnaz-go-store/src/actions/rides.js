import uuid from 'uuid'; //we import this to create a random id

// ADD_RIDE
// This method allow us to add a new ride
export const addRide = (
  {
    driver = '',
    note = '',
    outside_destination = '',
    inside_destination  = '',
    places = 0,
    createdAt = 0
  } = {}
) => ({
  type: 'ADD_RIDE',
  ride: {
    id: uuid(),
    driver,
    note,
    outside_destination,
    inside_destination,
    places,
    createdAt
  }
});

// REMOVE_RIDE
// This method allow us to remove an existing ride
export const removeRide = ({ id } = {}) => ({
  type: 'REMOVE_RIDE',
  id
});

// EDIT_RIDE
// This method allow us to edit a ride
export const editRide = (id, updates) => ({
  type: 'EDIT_RIDE',
  id,
  updates
});
