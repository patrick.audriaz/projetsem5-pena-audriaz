import moment from 'moment';

// Filters Reducer
const filtersReducerDefaultState = {
  text: '',
  sortBy: 'date',
  places: 0,
  outside_destination: 'Fribourg',
  inside_destination: 'Sonnaz',
  startDate: moment().startOf('month'),
  endDate: moment().endOf('month')
};

export default (state = filtersReducerDefaultState, action) => {
  switch (action.type) {
    case 'SET_TEXT_FILTER':
      return {
        ...state,
        text: action.text
      };
    case 'SORT_BY_PLACES':
      return {
        ...state,
        sortBy: 'places'
      };
    case 'SORT_BY_DATE':
      return {
        ...state,
        sortBy: 'date'
      };
    case 'SET_START_DATE':
      return {
        ...state,
        startDate: action.startDate
      };
    case 'SET_END_DATE':
      return {
        ...state,
        endDate: action.endDate
      };
    case 'SET_OUTSIDE_DESTINATION':
      return {
        ...state,
        endDate: action.outside_destination
      };
    case 'SET_INSIDE_DESTINATION':
      return {
        ...state,
        endDate: action.inside_destination
      };
    default:
      return state;
  }
};
