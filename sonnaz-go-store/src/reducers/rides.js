// Rides Reducer

const ridesReducerDefaultState = [];

export default (state = ridesReducerDefaultState, action) => {
  switch (action.type) {
    case 'ADD_RIDE':
      return [
        ...state,
        action.ride
      ];
    case 'REMOVE_RIDE':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_RIDE':
      return state.map((ride) => {
        if (ride.id === action.id) {
          return {
            ...ride,
            ...action.updates
          };
        } else {
          return ride;
        };
      });
    default:
      return state;
  }
};
