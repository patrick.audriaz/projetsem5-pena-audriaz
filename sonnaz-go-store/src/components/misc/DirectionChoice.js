import React from 'react';
import {NavLink} from 'react-router-dom';

const DirectionChoice = () =>(
    <div>
    <p>Direction Choice </p>
    <NavLink to="/add/2" activeClassName="is-active" >Inside Choice</NavLink>
    <NavLink to="/add/3" activeClassName="is-active" >Outside Choice</NavLink>
    </div>
);

export default DirectionChoice;