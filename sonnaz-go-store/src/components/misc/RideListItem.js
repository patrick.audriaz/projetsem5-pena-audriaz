import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { removeRide } from '../../actions/rides';

const RideListItem = ({ dispatch, id, note, driver, inside_destination, outside_destination, places, createdAt }) => (
  <div>
    <h3>Your driver is {driver}</h3>
    <p>Inside Destination: {inside_destination}</p>
    <p>Outside Destination: {outside_destination}</p>
    <p>Number of places avaibles: {places}</p>
    <p>Date: {createdAt}</p>
    <p>Additional notes: {note}</p>
    <button onClick={() => {
      dispatch(removeRide({ id }));
    }}>Remove</button>
    <button>
    <Link to={`/edit/${id}`}>Edit</Link>
    </button>
  </div>

);

export default connect()(RideListItem);
