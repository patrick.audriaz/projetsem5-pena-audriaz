import React from 'react';
import {NavLink} from 'react-router-dom';
import Icon from '@material-ui/core/Icon';

const AddRide = () =>(
    <div>
    <NavLink to="/add/1" activeClassName="is-active" ><Icon>add_circle</Icon></NavLink>
    </div>
);

export default AddRide;