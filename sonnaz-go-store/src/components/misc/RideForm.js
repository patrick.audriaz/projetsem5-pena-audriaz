import React, { Component } from 'react';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

export default class RideForm extends Component{
  constructor(props){
    super(props);
    this.state = {
      driver: props.ride ? props.ride.driver : '',
      note: props.ride ? props.ride.note : '',
      places: props.ride ? props.ride.places.toString() : '',
      outside_destination: props.ride ? props.ride.outside_destination.toString() : '',
      inside_destination: props.ride ? props.ride.inside_destination.toString() : '',
      createdAt: props.ride ? moment(props.ride.createdAt) : moment(),
      calendarFocused: false,
      error: ''
    };
  };
  onDriverChange = (e) => {
    const driver = e.target.value;
    this.setState(() => ({ driver }));
  };
  onOutsideChange = (e) => {
    const outside_destination = e.target.value;
    this.setState(() => ({ outside_destination }));
  };
  onInsideChange = (e) => {
    const inside_destination = e.target.value;
    this.setState(() => ({ inside_destination }));
  };
  onNoteChange = (e) => {
    const note = e.target.value;
    this.setState(() => ({ note }));
  };
  onPlacesChange = (e) => {
    const places = e.target.value;

    if (!places || places.match(/^\d{1,}(\.\d{0,2})?$/)) {
      this.setState(() => ({ places }));
    }
  };
  onDateChange = (createdAt) => {
    if(createdAt){
      this.setState(() => ({ createdAt }));
    }
  };
  onFocusChange = ({ focused }) => {
    this.setState(() => ({ calendarFocused: focused }));
  };
  onSubmit = (e) => {
    e.preventDefault();
    if (!this.state.driver || !this.state.places || !this.state.outside_destination ||!this.state.inside_destination) {
      this.setState(() =>({ error: 'Please provide driver, number of places and boths destinations values' }));
    } else {
      this.setState(()=>({ error: '' }));
      this.props.onSubmit({
      driver: this.state.driver,
      outside_destination: this.state.outside_destination,
      inside_destination: this.state.inside_destination,
      places: this.state.places,
      createdAt: this.state.createdAt.valueOf(),
      note: this.state.note   
      });
    }
  };
  render() {
    return (
      <div>
        {this.state.error && <p>{this.state.error}</p>}
        <form onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="Driver"
            autoFocus
            value={this.state.driver}
            onChange={this.onDriverChange}
          />
          <input
            type="text"
            placeholder="Outside Destination"
            autoFocus
            value={this.state.outside_destination}
            onChange={this.onOutsideChange}
          />
          <input
            type="text"
            placeholder="Inside Destination"
            autoFocus
            value={this.state.inside_destination}
            onChange={this.onInsideChange}
          />
          <input
            type="text"
            placeholder="Number of Places"
            value={this.state.places}
            onChange={this.onPlacesChange}
          />
          <SingleDatePicker
            date={this.state.createdAt}
            onDateChange={this.onDateChange}
            focused={this.state.calendarFocused}
            onFocusChange={this.onFocusChange}
            numberOfMonths={1}
            isOutsideRange={() => false}
          />
          <textarea
            placeholder="Add a note for your rid (optional)"
            value={this.state.note}
            onChange={this.onNoteChange}
          >
          </textarea>
          <button>Add Ride</button>
        </form>
      </div>
    )
  }
}
