import React from 'react';
import {NavLink} from 'react-router-dom';

const OutsideChoice = () =>(
    <div>
    <p>Outside Choice </p>
    <NavLink to="/add/1" activeClassName="is-active" > prev </NavLink>
    <NavLink to="/add/2" activeClassName="is-active" > next </NavLink>
    </div>
);

export default OutsideChoice;