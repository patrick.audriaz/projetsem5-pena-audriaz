import React from 'react';
import { connect } from 'react-redux';
import RideListItem from './RideListItem';
import selectRides from '../../selectors/rides';

const RideList = (props) => (
  <div>
    <h1>Ride's List</h1>
    {props.rides.map((ride) => {
      return <RideListItem key={ride.id} {...ride} />;
    })}
  </div>
);

const mapStateToProps = (state) => {
  return {
    rides: selectRides(state.rides, state.filters)
  };
};

export default connect(mapStateToProps)(RideList);
