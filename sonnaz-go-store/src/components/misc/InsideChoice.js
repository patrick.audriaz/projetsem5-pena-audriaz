import React from 'react';
import {NavLink} from 'react-router-dom';

const InsideChoice = () =>(
    <div>
    <p>Inside Choice </p>
    <NavLink to="/add/1" activeClassName="is-active" > prev </NavLink>
    <NavLink to="/add/3" activeClassName="is-active" > next </NavLink>
    </div>
);

export default InsideChoice;