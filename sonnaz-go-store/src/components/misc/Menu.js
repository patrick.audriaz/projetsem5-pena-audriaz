import React from 'react';
import { NavLink } from 'react-router-dom';

const Menu = () => (
  <div>
    <NavLink to="/profil" activeClassName="is-active">Profil</NavLink>
    <NavLink to="/contact" activeClassName="is-active">Contact</NavLink>
    <NavLink to="/" activeClassName="is-active" exact={true}>Logout</NavLink>  
  </div>
);

export default Menu;
