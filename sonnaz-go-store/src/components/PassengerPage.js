import React from 'react';
import RideList from './misc/RideList';
import RideListFilters from './misc/RideListFilters';
import BookRide from './misc/BookRide';
import Menu from './misc/Menu';

const PassengerPage = () => (
    <div>
        <Menu />
        <p>Passenger Page</p>
        <RideListFilters />
        <RideList />
        <BookRide />
    </div>
);

export default PassengerPage;