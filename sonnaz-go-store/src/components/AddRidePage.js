import React from 'react';
import { connect } from 'react-redux';
import RideForm from './misc/RideForm';
import { addRide } from '../actions/rides';
import Menu from './misc/Menu';

const AddRidePage = (props) => (
  <div>
    <Menu />
    <h1>Add Ride Page</h1>
    <RideForm
      onSubmit={(ride) =>{
        props.dispatch(addRide(ride));
        props.history.push('/driver');
      }}
    />
  </div>
);

export default connect()(AddRidePage);
