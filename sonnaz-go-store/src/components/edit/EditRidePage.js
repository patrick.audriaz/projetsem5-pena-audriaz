import React from 'react';
import { connect } from 'react-redux';
import RideForm from '../misc/RideForm';
import { editRide, removeRide } from '../../actions/rides';
import { Menu } from '@material-ui/core';

const EditRidePage = (props) => {
  return (
    <div>
      <Menu />
      <RideForm
        ride={props.ride}
         onSubmit={(ride) =>{
          props.dispatch(editRide(props.ride.id, ride));
          props.history.push('/driver');
        }} 
      />
      <button onClick={() => {
        props.dispatch(removeRide({ id: props.ride.id }));
        props.history.push('/driver');
      }}>Remove</button>
    </div>
  );
};

const mapStateToProps = (state, props) => {
  return {
    ride: state.rides.find((ride) => ride.id === props.match.params.id)
  }
}

export default connect(mapStateToProps)(EditRidePage);
