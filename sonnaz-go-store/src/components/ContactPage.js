import React from 'react';
import Menu from './misc/Menu';

const ContactPage = () =>(
    <div>
        <Menu />
        <p>Contact Page</p>
    </div>
);

export default ContactPage;