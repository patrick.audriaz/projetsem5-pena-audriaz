import React from 'react';
import RideList from './misc/RideList';
import RideListFilters from './misc/RideListFilters';
import AddRide from './misc/AddRide';
import Menu from './misc/Menu';
import AlertDialog from './AlertDialog';

const DriverPage = () => (
  <div>
    <Menu />
    <RideListFilters />
    <RideList />
    <AddRide />
    <AlertDialog />
  </div>
);

export default DriverPage;
