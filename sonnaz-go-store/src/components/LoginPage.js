import React from 'react';
import { NavLink } from 'react-router-dom';

const LoginPage = () => (
  <div>
    <NavLink to="/driver" activeClassName="is-active">Driver</NavLink>
    <NavLink to="/passenger" activeClassName="is-active">Passenger</NavLink>
  </div>
);

export default LoginPage;