import React from 'react';
import InsideChoice from '../misc/InsideChoice';
import { Menu } from '@material-ui/core';

const BookRidePage1 = () =>(
    <div>
        <Menu />
        <p>Book Ride Page 1 </p>
        <InsideChoice />
    </div>
);

export default BookRidePage1;