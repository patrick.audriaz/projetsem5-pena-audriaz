import React from 'react';
import OutsideChoice from '../misc/OutsideChoice';
import { Menu } from '@material-ui/core';

const BookRidePage2 = () =>(
    <div>
        <Menu />
        <p>Book Ride Page 2 </p>
        <OutsideChoice />
    </div>
);

export default BookRidePage2;