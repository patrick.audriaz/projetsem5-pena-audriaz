import React from 'react';
import Menu from '../misc/Menu';
import { connect } from 'react-redux';
import selectMatching from '../../selectors/matching';
import { NavLink } from 'react-router-dom';
import MatchingRides from '../misc/MatchingRides';

const BookRidePage4 = (props) => (
  <div>
    <Menu />
        <h1>Book Ride Page 2</h1>
        <p>Inside destination: {props.filters.insideValue}</p>
        <p>Outside destination: {props.filters.outsideValue}</p>
        <p>Date: {props.filters.createdAt.toString()}</p>
        <MatchingRides />
        <NavLink to="/book/3" activeClassName="is-active" > prev </NavLink>
  </div>
);

const mapStateToProps = (state) => {
  return {
    rides: selectMatching(state.rides, state.filters),
    filters: state.filters
  };
};

export default connect(mapStateToProps)(BookRidePage4);
