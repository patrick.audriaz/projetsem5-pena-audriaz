import React from 'react';
import DirectionChoice from '../misc/DirectionChoice';
import { Menu } from '@material-ui/core';

const AddRidePage1 = () => (
  <div>
    <Menu />
    <h1>Add Ride Page 1</h1>
    <DirectionChoice />
  </div>
);

export default AddRidePage1;