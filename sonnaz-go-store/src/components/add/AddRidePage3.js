import React from 'react';
import { connect } from 'react-redux';
import RideForm from '../misc/RideForm';
import { addRide } from '../../actions/rides';
import OutsideChoice from '../misc/OutsideChoice';
import { Menu } from '@material-ui/core';

const AddRidePage3 = (props) => (
  <div>
    <Menu />
    <h1>Add Ride Page 3</h1>
    <RideForm
      onSubmit={(ride) =>{
        props.dispatch(addRide(ride));
        props.history.push('/driver');
      }}
    />
    <OutsideChoice />
  </div>
);

export default connect()(AddRidePage3);