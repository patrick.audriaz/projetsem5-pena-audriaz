import React from 'react';
import { connect } from 'react-redux';
import RideForm from '../misc/RideForm';
import { addRide } from '../../actions/rides';
import InsideChoice from '../misc/InsideChoice';
import Menu from '../misc/Menu';

const AddRidePage2 = (props) => (
  <div>
    <Menu />
    <h1>Add Ride Page 2</h1>
    <RideForm
      onSubmit={(ride) =>{
        props.dispatch(addRide(ride));
        props.history.push('/driver');
      }}
    />
    <InsideChoice />
  </div>
);

export default connect()(AddRidePage2);