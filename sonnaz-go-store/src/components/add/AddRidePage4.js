import React, { Component } from 'react';
import Menu from '../misc/Menu';
import RideForm from '../misc/RideForm';
import { addRide } from '../../actions/rides';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

const AddRidePage4 = (props) => {
    return (
      <div>
        <Menu />
        <h1>Add Ride Page 4</h1>
        <RideForm
            driver={props.filters.driverName}
            outside={props.filters.outsideValue}
            inside={props.filters.insideValue}
            onSubmit={(ride) =>{
            props.dispatch(addRide(ride));
            props.history.push('/driver');
          }}
        />
        <NavLink to="/add/3" activeClassName="is-active" > prev </NavLink>
      </div>
    );
}

const mapStateToProps = (state) => {
  return {
    filters: state.filters
  };
};

export default connect(mapStateToProps)(AddRidePage4);