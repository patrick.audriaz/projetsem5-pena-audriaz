import moment from 'moment';

// Get visible rides
export default (rides, { text, sortBy, startDate, endDate }) => {
  return rides.filter((ride) => {
    const createdAtMoment = moment(ride.createdAt);
    const startDateMatch = startDate ? startDate.isSameOrBefore(createdAtMoment, 'day') :true;
    const endDateMatch = endDate ? endDate.isSameOrAfter(createdAtMoment, 'day') : true;
    const textMatch = ride.driver.toLowerCase().includes(text.toLowerCase());
    
    return startDateMatch && endDateMatch && textMatch;
  }).sort((a, b) => {
    if (sortBy === 'date') {
      return a.createdAt < b.createdAt ? 1 : -1;
    } else if (sortBy === 'places') {
      return a.places < b.places ? 1 : -1;
    }
  });
};
