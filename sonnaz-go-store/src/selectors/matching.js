//Get matching rides
export default (rides, { insideValue, outsideValue }) => {
  return rides.filter((ride) => {
    const insideMatch = ride.driver.toLowerCase().includes(insideValue.toLowerCase());
    const outsideMatch = ride.driver.toLowerCase().includes(outsideValue.toLowerCase());
    return insideMatch && outsideMatch;
  });
};