import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
import { addRide } from './actions/rides';
import 'normalize.css/normalize.css';
import './styles/styles.scss';

const store = configureStore();

//Adding some test values to the store 
store.dispatch(addRide({ driver: 'Nestor Peña', places: 4, outside_destination:'Fribourg 1', inside_destination: 'Sonnaz 1', createdAt:10000}));
store.dispatch(addRide({ driver: 'Patrick Audriaz', places: 3, outside_destination:'Fribourg 2', inside_destination: 'Sonnaz 2', createdAt: 20000}));
store.dispatch(addRide({ driver: 'Sandy Ingram', places: 2, outside_destination:'Fribourg 3', inside_destination: 'Sonnaz 3', createdAt: 30000 }));
store.dispatch(addRide({ driver: 'Philippe Joye', places: 2,outside_destination:'Fribourg 4', inside_destination: 'Sonnaz 4', createdAt: 40000 }));

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

ReactDOM.render(jsx, document.getElementById('app'));
