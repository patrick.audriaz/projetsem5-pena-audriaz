import React from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import LoginPage from '../components/LoginPage';
import DriverPage from '../components/DriverPage';
import PassengerPage from '../components/PassengerPage';
import AddRidePage1 from '../components/add/AddRidePage1';
import AddRidePage2 from '../components/add/AddRidePage2';
import AddRidePage3 from '../components/add/AddRidePage3';
import BookRidePage1 from "../components/book/BookRidePage1";
import BookRidePage2 from "../components/book/BookRidePage2";
import BookRidePage3 from "../components/book/BookRidePage3";
import EditRidePage from '../components/edit/EditRidePage';
import ContactPage from '../components/ContactPage';
import ProfilPage from '../components/ProfilPage';
import NotFoundPage from '../components/NotFoundPage';

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Switch>
        <Route path="/" component={LoginPage} exact={true} />
        <Route path="/driver" component={DriverPage}  />
        <Route path="/passenger" component={PassengerPage}  />
        <Route path="/add/1" component={AddRidePage1} />
        <Route path="/add/2" component={AddRidePage2} />
        <Route path="/add/3" component={AddRidePage3} />
        <Route path="/book/1" component={BookRidePage1} />
        <Route path="/book/2" component={BookRidePage2} />
        <Route path="/book/3" component={BookRidePage3} />
        <Route path="/edit/:id" component={EditRidePage} />
        <Route path="/profil" component={ProfilPage} />
        <Route path="/contact" component={ContactPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default AppRouter;
